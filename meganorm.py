import jax.numpy as jnp
import datatable as dt
import pandas as pd
import numpy as np
from conorm import cpm
from collections import defaultdict
from jax import jit, grad
from jax.scipy.optimize import minimize
from jax.scipy.special import logsumexp
from jax.config import config
config.update("jax_enable_x64", True)

filename = 'results/adjusted_counts.tsv'
df = dt.fread(filename).to_pandas().set_index('Gene')
df = df.loc[np.all(df != 0, axis=1)]
hs = set(dt.fread('hs.tsv').to_pandas().values.flatten())
# df = cpm(df, norm_factors='TMM')
hours = ['0h', '4h', '8h']
lines = set()
cols = list(df.columns)
dcols = defaultdict(lambda: defaultdict(list))
ncols = list()
for c in cols:
    its = c.split('_')
    h = its[-2]
    if h not in hours:
        continue
    line = '_'.join(its[:-2])
    dcols[line][h].append(c)
    ncols.append(c)

df = df[ncols]
tdf = dt.fread('BRIC_gene_counts_wgt.tsv').to_pandas().set_index('gene')
gene_name = tdf['gene_name'].loc[df.index]
del tdf
hk = [g in hs for g in gene_name.values]
gene_names = dict()
for g, n in gene_name.iteritems():
    gene_names[g] = n
    
leave = np.array(hk) | (df.min(axis=1) > 1000)
df = df.loc[leave]
time = dict()
groups = dict()
groups_m = defaultdict(list)
time_m = defaultdict(list)
cols = list(df.columns)
for c in cols:
    its = c.split('_')
    time[c] = float(its[-2][:-1])
    groups[c] = '_'.join(its[:-2])
    groups_m[groups[c]].append(c)
    time_m[time[c]].append(c)

h0_cols = list(filter(lambda x: time[x] == 0, cols))
df_h0 = cpm(df[h0_cols], norm_factors='TMM')
hp_cols = list(filter(lambda x: x not in h0_cols, cols))
df_hp = cpm(df[hp_cols])
group_names = sorted(groups_m, key=lambda x: len(str(x)))
time_names = sorted(time_m)
cols = sorted(cols, key=lambda x: (group_names.index(groups[x]), time_names.index(time[x])))
df = pd.concat([df_h0, df_hp], axis=1)[cols]
df_hp = np.log(df_hp[[c for c in df.columns if c in df_hp.columns]])
for line, cols in groups_m.items():
    csub = list(filter(lambda x: x in cols, df_h0.columns))
    N0 = np.log(df_h0[csub].mean(axis=1))
    csub = list(filter(lambda x: x in cols, df_hp.columns))
    df_hp[csub] = df_hp[csub].sub(N0, axis=0)
from jax.scipy.special import logsumexp


sub_mxs = list()
sub_times = list()
for line, cols in groups_m.items():
    csub = list(filter(lambda x: x in cols, df_hp.columns))
    sub_mxs.append(jnp.array(df_hp[csub].values))
    sub_times.append(jnp.array([time[c] for c in csub]))
var = np.log(df).var().mean()
# del df_hp
# del df
del df_h0

@jit
def smooth_max(vals, a=0.0):
    return logsumexp(a * vals, b=vals) - logsumexp(a * vals)

@jit
def smooth_max2(vals, a=0.0):
    exps = jnp.exp(a * vals)
    return jnp.sum(vals * exps) / exps.sum(axis=0)
xt = list()
for i in range(100):
    x = jnp.array(np.random.normal(size=100))
    v0 = smooth_max(x, -1)
    v1 = smooth_max2(x, -1)
    xt.append(abs(v0 - v1))
    

def smooth_max_grad(vals, a=0.0):
    exps = np.exp(a * vals)
    sm = exps.sum(axis=0)
    s = np.sum(exps * vals) / sm
    return np.array([1 + a * (x - s) for x in vals]) * exps / sm
    

# def diff(y: np.ndarray, factors: np.ndarray, times: np.ndarray,
#          rates: np.ndarray):
#     d = y - factors - np.outer(rates ** 2, times)
#     return (d ** 2).mean(axis=1)
@jit
def diff(y: jnp.ndarray, factors: jnp.ndarray, times: jnp.ndarray,
         rates: jnp.ndarray):
    d = y - factors + jnp.outer(rates ** 2, times)
    return (d ** 2).mean(axis=1)


# def create_param_vector(sub_mxs):
#     rates = list()
#     factors = list()
#     param = list()
#     k = 0
#     for mx in sub_mxs:
#         n, m = mx.shape
#         factors.append(slice(k, m + k))
#         param.extend([0.0] * m)
#         k += m
#         rates.append(slice(k, k + n))
#         param.extend(([2.0] * n))
#         k += n
#         break
#     return np.array(param), factors, rates
def create_param_vector(sub_mxs):
    # rates = list()
    factors = list()
    param = list()
    k = 0
    for mx in sub_mxs:
        n, m = mx.shape
        factors.append(slice(k, m + k))
        param.extend([0.0] * m)
        k += m
        # rates.append(slice(k, k + n))
        # param.extend(([2] * n))
        # k += n
        # break
    rates = slice(k, k + n)
    param.extend([0.1] * n)
    return jnp.array(param), factors, rates
x0, factors, rates = create_param_vector(sub_mxs)


def loss(x, times, factors, rates, data):
    long_res = np.zeros(data[0].shape[0])
    c = 0
    for mx, time, factor in zip(data, times, factors):
        factor = x[factor]
        rate = x[rates]
        long_res += diff(mx, factor, time, rate)
        c += 1
    long_res /= data[0].shape[1]
    return smooth_max(long_res, -100)#-logsumexp(-long_res * 2) / 2
# def loss(x, times, factors, rates, data):
#     long_res = None
#     for mx, time, factor, rate in zip(data, times, factors, rates):
#         factor = x[factor]
#         rate = x[rate]
#         d = diff(mx, factor, time, rate)
#         if long_res is None:
#             long_res = d
#         else:
#             long_res = np.append(long_res, d)
#     return smooth_max(long_res, -0.1)

t = loss(x0, sub_times, factors, rates, sub_mxs)
# from scipy.optimize import minimize
fun = lambda x: loss(x, sub_times, factors, rates, sub_mxs)
f0 = fun(x0)
opt = minimize(fun, x0, method="BFGS")
nfs = list()
rts = list()
# for f, r in zip(factors, rates):
#     nfs.extend(opt.x[f])
#     rts.extend(opt.x[r])
for f in factors:
    nfs.extend(opt.x[f])
rts = opt.x[rates] ** 2
nfs = jnp.append(jnp.array(nfs), np.zeros( 1  * 3))
nfs -= nfs.mean()
nfs = jnp.exp(nfs)
nfs = pd.DataFrame(nfs, index=list(df_hp.columns) + h0_cols).loc[df.columns]
# col    adj = 0rate
# nfs = np.exp(nfs)

    

# def big_loss


