library(sva)
args = commandArgs(trailingOnly=TRUE)
groups = rapply(strsplit(args[5], ";"), as.integer)
if (all(is.na(groups)))
{
  groups = NULL
}
print("Arguments:"); print(cat(args, sep="|"))
cols = rapply(strsplit(args[4], ';'), as.integer)
batches = rapply(strsplit(args[2], ";"), as.integer)
batches = batches + 1 - min(batches)
counts = data.matrix(read.table(args[1], row.names=1, sep='\t', header=T))
if (!all(is.na(cols)))
{
  counts = counts[, cols]
} 
print(colnames(counts))
print("Batches:")
print(batches)
print("Groups:")
print(groups)
adjustedCounts = ComBat_seq(counts, batch=batches, group=groups)
write.table(adjustedCounts, args[3], sep='\t', col.names=NA)