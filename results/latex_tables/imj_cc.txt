\begin{tabular}{lllrrrrrrrrrrrr}
\toprule
             &                      &       & \multicolumn{12}{c}{Case-control} \\
             &                      &       & \multicolumn{2}{c}{dd\_pYB1-dd} & \multicolumn{2}{c}{dd\_pYB1\_pYB3-dd} & \multicolumn{2}{c}{dd\_pYB3-dd} & \multicolumn{2}{c}{dd\_pYB1\_pYB3-dd\_pYB1} & \multicolumn{2}{c}{dd\_pYB3-dd\_pYB1} & \multicolumn{2}{c}{dd\_pYB3-dd\_pYB1\_pYB3} \\
             &                      &       &    ImpulseDE & edgeR &       ImpulseDE & edgeR &  ImpulseDE & edgeR &            ImpulseDE & edgeR &       ImpulseDE & edgeR &            ImpulseDE & edgeR \\
\midrule
\multirow{12}{*}{Case-control} & \multirow{2}{*}{dd\_pYB1-dd} & ImpulseDE &        66.60 & 44.45 &           43.85 & 31.04 &      70.34 & 51.86 &                53.59 & 35.47 &           53.39 & 42.93 &                57.05 & 36.20 \\
             &                      & edgeR &           & 56.11 &           34.15 & 32.26 &      49.17 & 43.65 &                38.00 & 42.02 &           40.39 & 33.30 &                44.72 & 29.06 \\
\cline{2-15}
             & \multirow{2}{*}{dd\_pYB1\_pYB3-dd} & ImpulseDE &           &    &           43.39 & 55.53 &      43.09 & 34.41 &                41.69 & 33.80 &           33.57 & 28.09 &                42.49 & 33.23 \\
             &                      & edgeR &           &    &              & 33.44 &      31.77 & 31.36 &                32.35 & 26.50 &           24.47 & 21.61 &                32.35 & 30.31 \\
\cline{2-15}
             & \multirow{2}{*}{dd\_pYB3-dd} & ImpulseDE &           &    &              &    &      74.63 & 56.73 &                53.65 & 37.56 &           60.68 & 45.57 &                66.93 & 39.55 \\
             &                      & edgeR &           &    &              &    &         & 49.03 &                37.50 & 30.87 &           41.23 & 31.94 &                44.58 & 36.94 \\
\cline{2-15}
             & \multirow{2}{*}{dd\_pYB1\_pYB3-dd\_pYB1} & ImpulseDE &           &    &              &    &         &    &                58.77 & 40.54 &           49.00 & 38.97 &                64.25 & 49.70 \\
             &                      & edgeR &           &    &              &    &         &    &                   & 44.21 &           32.57 & 28.25 &                43.72 & 36.91 \\
\cline{2-15}
             & \multirow{2}{*}{dd\_pYB3-dd\_pYB1} & ImpulseDE &           &    &              &    &         &    &                   &    &           61.95 & 43.83 &                61.04 & 35.51 \\
             &                      & edgeR &           &    &              &    &         &    &                   &    &              & 48.52 &                44.83 & 31.17 \\
\cline{2-15}
             & \multirow{2}{*}{dd\_pYB3-dd\_pYB1\_pYB3} & ImpulseDE &           &    &              &    &         &    &                   &    &              &    &                72.01 & 53.88 \\
             &                      & edgeR &           &    &              &    &         &    &                   &    &              &    &                   & 42.93 \\
\bottomrule
\end{tabular}
