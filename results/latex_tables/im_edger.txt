\begin{tabular}{lllrrrrrrrrrr}
\toprule
             &                      &       & \multicolumn{4}{c}{Case-only} & \multicolumn{6}{c}{Case-control} \\
             &                      &       &        dd & dd\_pYB1 & dd\_pYB1\_pYB3 & dd\_pYB3 &   dd\_pYB1-dd & dd\_pYB1\_pYB3-dd & dd\_pYB3-dd & dd\_pYB1\_pYB3-dd\_pYB1 & dd\_pYB3-dd\_pYB1 & dd\_pYB3-dd\_pYB1\_pYB3 \\
             &                      &       &     edgeR &   edgeR &        edgeR &   edgeR &        edgeR &           edgeR &      edgeR &                edgeR &           edgeR &                edgeR \\
\midrule
\multirow{4}{*}{Case-only} & dd & edgeR &     13360 &   13201 &        12875 &   13292 &         7601 &            4424 &       6596 &                 5896 &            6576 &                 5743 \\
             & dd\_pYB1 & edgeR &        &   13470 &        12973 &   13403 &         7553 &            4490 &       6615 &                 5935 &            6563 &                 5772 \\
             & dd\_pYB1\_pYB3 & edgeR &        &      &        13132 &   13069 &         7397 &            4282 &       6473 &                 5906 &            6532 &                 5630 \\
             & dd\_pYB3 & edgeR &        &      &           &   13591 &         7628 &            4547 &       6650 &                 6011 &            6625 &                 5833 \\
\cline{1-13}
\multirow{6}{*}{Case-control} & dd\_pYB1-dd & edgeR &        &      &           &      &         7675 &            2988 &       4370 &                 4060 &            3575 &                 3050 \\
             & dd\_pYB1\_pYB3-dd & edgeR &        &      &           &      &           &            4574 &       2693 &                 2225 &            1992 &                 2430 \\
             & dd\_pYB3-dd & edgeR &        &      &           &      &           &              &       6706 &                 3008 &            3230 &                 3393 \\
             & dd\_pYB1\_pYB3-dd\_pYB1 & edgeR &        &      &           &      &           &              &         &                 6047 &            2794 &                 3213 \\
             & dd\_pYB3-dd\_pYB1 & edgeR &        &      &           &      &           &              &         &                   &            6636 &                 2972 \\
             & dd\_pYB3-dd\_pYB1\_pYB3 & edgeR &        &      &           &      &           &              &         &                   &              &                 5872 \\
\bottomrule
\end{tabular}
