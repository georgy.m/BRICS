\begin{tabular}{lllrrrrrrrrrr}
\toprule
             &                      &           & \multicolumn{4}{c}{Case-only} & \multicolumn{6}{c}{Case-control} \\
             &                      &           &        dd &   dd\_pYB1 & dd\_pYB1\_pYB3 &   dd\_pYB3 &   dd\_pYB1-dd & dd\_pYB1\_pYB3-dd & dd\_pYB3-dd & dd\_pYB1\_pYB3-dd\_pYB1 & dd\_pYB3-dd\_pYB1 & dd\_pYB3-dd\_pYB1\_pYB3 \\
             &                      &           & ImpulseDE & ImpulseDE &    ImpulseDE & ImpulseDE &    ImpulseDE &       ImpulseDE &  ImpulseDE &            ImpulseDE &       ImpulseDE &            ImpulseDE \\
\midrule
\multirow{4}{*}{Case-only} & dd & ImpulseDE &      6505 &      2256 &         3353 &      3352 &         5586 &            3368 &       5798 &                 3943 &            4201 &                 4861 \\
             & dd\_pYB1 & ImpulseDE &        &      3985 &         2225 &      3053 &         3037 &            1681 &       3299 &                 2759 &            3218 &                 3209 \\
             & dd\_pYB1\_pYB3 & ImpulseDE &        &        &         6141 &      3059 &         4183 &            3392 &       4709 &                 5066 &            3910 &                 5398 \\
             & dd\_pYB3 & ImpulseDE &        &        &           &      6308 &         4631 &            2649 &       5283 &                 4077 &            5182 &                 5130 \\
\cline{1-13}
\multirow{6}{*}{Case-control} & dd\_pYB1-dd & ImpulseDE &        &        &           &        &         9109 &            4586 &       7977 &                 5983 &            6120 &                 6887 \\
             & dd\_pYB1\_pYB3-dd & ImpulseDE &        &        &           &        &           &            5935 &       4861 &                 4111 &            3621 &                 4707 \\
             & dd\_pYB3-dd & ImpulseDE &        &        &           &        &           &              &      10208 &                 6371 &            7055 &                 8042 \\
             & dd\_pYB1\_pYB3-dd\_pYB1 & ImpulseDE &        &        &           &        &           &              &         &                 8038 &            5430 &                 6997 \\
             & dd\_pYB3-dd\_pYB1 & ImpulseDE &        &        &           &        &           &              &         &                   &            8473 &                 6945 \\
             & dd\_pYB3-dd\_pYB1\_pYB3 & ImpulseDE &        &        &           &        &           &              &         &                   &              &                 9850 \\
\bottomrule
\end{tabular}
