\begin{tabular}{lllrrrrrrrrrrrr}
\toprule
             &                      &       & \multicolumn{12}{c}{Case-control} \\
             &                      &       & \multicolumn{2}{c}{dd\_pYB1-dd} & \multicolumn{2}{c}{dd\_pYB1\_pYB3-dd} & \multicolumn{2}{c}{dd\_pYB3-dd} & \multicolumn{2}{c}{dd\_pYB1\_pYB3-dd\_pYB1} & \multicolumn{2}{c}{dd\_pYB3-dd\_pYB1} & \multicolumn{2}{c}{dd\_pYB3-dd\_pYB1\_pYB3} \\
             &                      &       &    ImpulseDE &   edgeR &       ImpulseDE &   edgeR &  ImpulseDE &   edgeR &            ImpulseDE &   edgeR &       ImpulseDE &   edgeR &            ImpulseDE &   edgeR \\
\midrule
\multirow{12}{*}{Case-control} & \multirow{2}{*}{dd\_pYB1-dd} & ImpulseDE &      9109.00 & 5165.00 &         4586.00 & 3241.00 &    7977.00 & 5401.00 &              5983.00 & 3968.00 &         6120.00 & 4729.00 &              6887.00 & 3982.00 \\
             &                      & edgeR &           & 7675.00 &         3465.00 & 2988.00 &    5895.00 & 4370.00 &              4327.00 & 4060.00 &         4646.00 & 3575.00 &              5415.00 & 3050.00 \\
\cline{2-15}
             & \multirow{2}{*}{dd\_pYB1\_pYB3-dd} & ImpulseDE &           &      &         5935.00 & 3752.00 &    4861.00 & 3236.00 &              4111.00 & 3027.00 &         3621.00 & 2757.00 &              4707.00 & 2945.00 \\
             &                      & edgeR &           &      &              & 4574.00 &    3564.00 & 2693.00 &              3083.00 & 2225.00 &         2565.00 & 1992.00 &              3526.00 & 2430.00 \\
\cline{2-15}
             & \multirow{2}{*}{dd\_pYB3-dd} & ImpulseDE &           &      &              &      &   10208.00 & 6122.00 &              6371.00 & 4438.00 &         7055.00 & 5273.00 &              8042.00 & 4557.00 \\
             &                      & edgeR &           &      &              &      &         & 6706.00 &              4021.00 & 3008.00 &         4431.00 & 3230.00 &              5105.00 & 3393.00 \\
\cline{2-15}
             & \multirow{2}{*}{dd\_pYB1\_pYB3-dd\_pYB1} & ImpulseDE &           &      &              &      &         &      &              8038.00 & 4063.00 &         5430.00 & 4115.00 &              6997.00 & 4618.00 \\
             &                      & edgeR &           &      &              &      &         &      &                   & 6047.00 &         3567.00 & 2794.00 &              4836.00 & 3213.00 \\
\cline{2-15}
             & \multirow{2}{*}{dd\_pYB3-dd\_pYB1} & ImpulseDE &           &      &              &      &         &      &                   &      &         8473.00 & 4604.00 &              6945.00 & 3759.00 \\
             &                      & edgeR &           &      &              &      &         &      &                   &      &              & 6636.00 &              5103.00 & 2972.00 \\
\cline{2-15}
             & \multirow{2}{*}{dd\_pYB3-dd\_pYB1\_pYB3} & ImpulseDE &           &      &              &      &         &      &                   &      &              &      &              9850.00 & 5505.00 \\
             &                      & edgeR &           &      &              &      &         &      &                   &      &              &      &                   & 5872.00 \\
\bottomrule
\end{tabular}
