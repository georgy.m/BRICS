from matplotlib import pyplot as plt
import datatable as dt
import seaborn as sns
import pandas as pd
import numpy as np
import subprocess
import os
import re
from sklearn.decomposition import PCA
from collections import defaultdict
from itertools import combinations
from conorm import cpm
from umap import UMAP


seed = 1
drop_1h = True
filter_no_8h = True
translate_names = False
filter_mt = True
pval_cutoff = 0.05
edger_samples = '1;2;3'
edger_hours = "0;8"
edger_contrasts = "-1;1"
test_gene_counts = ['YBX1', 'YBX3']
np.random.seed(seed)
counts_filename = 'BRIC_gene_counts_wgt.tsv'
names_filename = 'sample_annotation.tsv'
batch_filename = 'Batch.txt'
output_folder = 'results'
tmp_folder = 'tmp'
os.makedirs(output_folder, exist_ok=True)
os.makedirs(tmp_folder, exist_ok=True)

annotation = pd.read_csv(names_filename, sep='\t', header=None)
adjusted_counts_filename = os.path.join(output_folder, 'adjusted_counts.tsv')
df = dt.fread(counts_filename).to_pandas().set_index('gene')
test_gene_counts_indices = [df[df.gene_name == n].index[0]
                            for n in test_gene_counts]
if filter_mt:
    t = [not x.startswith('MT-') for x in df['gene_name']]
    df = df.loc[t]



if 'gene_name' in df.columns:
    gene_name = df['gene_name']
    gene_type = df['gene_type']
    df = df.drop(['gene_name', 'gene_type'], axis=1)
if 'C0' in df.columns:
    df = df.drop(['C0'], axis=1)
if translate_names:
    mapping = dict()
    for _, r in annotation.iterrows():
        n = r[2].split('.fastq')[0]
        for c in r[1].split(','):
            c = c.split('.fastq')[0]
            mapping[c] = n
    to_rem = list()
    for c in df.columns:
        if c not in mapping:
            to_rem.append(c)
    df = df.drop(to_rem, axis=1)
    df.columns = list(map(mapping.get, df.columns))
    print('Those individuals were not found:')
    print('\n'.join(to_rem))
        
    
df = df.loc[filter(lambda x: not x.lower().startswith('spikein'), df.index)]
if drop_1h:
    df = df[filter(lambda x: '1h' not in x, df.columns)]
    
def normalize(df, use_custom_norm=True):
    if not use_custom_norm:
        return cpm(df, norm_factors='TMM')
    norm_factors = pd.read_csv("results/norm_factors.tsv", sep='\t',
                               index_col=0).loc[df.columns]
    return cpm(df, norm_factors=norm_factors)
    
    
# df = cpm(df, norm_factors='TMM')

embder = UMAP(3, random_state=seed)
embeddings = {'UMAP': UMAP(3, random_state=seed),
              'PCA': PCA(2)}
# embeddings.clear()

markers = {0: 'o', 1: '^',  4: 's', 8: '+'}
colors = {1: 'r', 2: 'g', 3: 'b'}
cols = list(df.columns)
# cols = list(filter(lambda x: '1h' not in x, cols))
# df = df[cols]
ptrn = re.compile('(\w+)_(\d)h_(\d)')
d = defaultdict(list)
for col in cols:
    name, h, s = ptrn.findall(col)[0]
    d[name].append((col, int(h), colors[int(s)]))
to_remain = np.ones(len(df), dtype=bool)
for name, its in d.items():
    cs = [c[0] for c in its if not filter_no_8h or '8h' not in c[0]]
    # to_remain &= df[cs].std(axis=1) > 0
    to_remain &= np.all(df[cs] > 0, axis=1)
df = df.loc[to_remain]
print("Adjusting counts for batch-effects...")
batch = dt.fread(batch_filename).to_pandas().set_index('Код')
cs = list()
groups = list()
for c in cols:
    # groups.append(c.split('_')[-1])
    c = '_'.join(c.split('_')[:-1])
    try:
        i = cs.index(c) + 1
    except ValueError:
        cs.append(c)
        i = len(cs)
    groups.append(str(i))
batches = [0] * len(cols)
batch_map = dict()
for _, row in batch.iterrows():
    b = int(row['Batch'][1:])
    col = row['Название'][2:].replace('+', '_').strip()
    its = col.split(' ')
    if len(its) == 3:
        its = [str()] + its
    its[0] = its[0].replace('YB', 'pYB')
    col = f'dd{its[0]}_{its[1]}_{its[3]}'
    try:
        batches[cols.index(col)] = str(b)
        batch_map[col] = b
    except ValueError:
        pass
td = defaultdict(list)
for c, b, g in zip(cols, batches, groups):
    # td[b].append('_'.join(c.split('_')[:-1]))
    td['_'.join(c.split('_')[:-1])].append(b)
    
batches = np.array(batches)
groups = np.array(groups)
tmp_filename = os.path.join(tmp_folder, 'tmp_out.tsv')
df.to_csv(tmp_filename, sep='\t')
df_adjusted = None
for gr, its in d.items():
    print(gr)
    inds = [cols.index(c[0]) for c in its]
    bs = ';'.join(batches[inds])
    grs = ';'.join(groups[inds])
    inds = ';'.join(map(lambda x: str(x + 1), inds))
    adjusted_tmp = os.path.join(tmp_folder, f'{gr}.tsv')
    subprocess.run(f'Rscript batches.r {tmp_filename} {bs} {adjusted_tmp} {inds} {grs}'.split(' '))
    t = dt.fread(adjusted_tmp).to_pandas()
    t = t.set_index(t.columns[0])
    t.index.name = 'Gene'
    if df_adjusted is None:
        df_adjusted = t
    else:
        df_adjusted = pd.concat([df_adjusted, t], axis=1)
    os.remove(adjusted_tmp)
os.remove(tmp_filename)
df_adjusted.to_csv(adjusted_counts_filename, sep='\t')
# df_adjusted = dt.fread(adjusted_counts_filename).to_pandas()
# df_adjusted = df_adjusted.set_index(df_adjusted.columns[0])

def plot_embeddings(df, prefix: str):
    legend_elements = [plt.scatter([0], [0], color='k', marker='o', label='0h'),
                       # plt.scatter([0], [0], color='k', marker='^', label='1h'),
                       plt.scatter([0], [0], color='k', marker='s', label='4h'),
                       plt.scatter([0], [0], color='k', marker='+', label='8h')]
    for emb_name, embder in embeddings.items():
        plt.figure(figsize=(10, 10), dpi=200)
        for j, (title, its) in enumerate(d.items()):
            xs = list(); ys = list(); colors = list(); ms = list()
            cols = list()
            for name, m, c in its:
                # i = cols.index(name)
                # x, y = feats[i]
                # xs.append(x); ys.append(y)
                cols.append(name)
                colors.append(c)
                ms.append(markers[m])
            subdf = normalize(df[cols]).T
            subdf -= subdf.mean()
            subdf /= subdf.std()
            feats = embder.fit_transform(subdf)
            xs = feats[:, 0]; ys = feats[:, 1]
            plt.subplot(2, 2, j + 1)
            for x, y, m, c in zip(xs, ys, ms, colors):
                plt.scatter(x, y, marker=m, c=c)
            # plt.xticks([])
            # plt.yticks([])
            xlabel = 'PC1'
            ylabel = 'PC2'
            try:
                v = embder.explained_variance_ratio_
                ev = '{:.2f}'.format(v[0] * 100)
                xlabel += f' ({ev}%)'
                ev = '{:.2f}'.format(v[1] * 100)
                ylabel += f' ({ev}%)'
                plt.xlabel(xlabel)
                plt.ylabel(ylabel)
            except AttributeError:
                if not j % 2:
                    plt.ylabel(ylabel)
                if j > 1:
                    plt.xlabel(xlabel)
            plt.title(title)
            if j == 1:
                plt.legend(handles=legend_elements, loc='lower right', ncol=2)
        plt.tight_layout()
        plt.savefig(os.path.join(output_folder, f'{prefix}{emb_name}.png'), dpi=200)
    pca = PCA(3)
    plt.figure(figsize=(10, 20), dpi=200)
    for j, (title, its) in enumerate(d.items()):
        xs = list(); ys = list(); colors = list(); ms = list()
        cols = list()
        for name, m, c in its:
            # i = cols.index(name)
            # x, y = feats[i]
            # xs.append(x); ys.append(y)
            cols.append(name)
            colors.append(c)
            ms.append(markers[m])
        subdf = normalize(df[cols])
        # subdf = df[cols]
        # print(subdf)
        print('Before:', subdf.shape)
        subdf = subdf.loc[np.any(subdf > 5, axis=1)]
        print('After:', subdf.shape)
        subdf = subdf.T
        stds = subdf.std()
        subdf -= subdf.mean()
        subdf /= stds
        feats = pca.fit_transform(subdf)
        xs = feats[:, 0]; ys = feats[:, 1]; zs = feats[:, 2]
        v = list(map(lambda x: '{:.2f}%'.format(x), pca.explained_variance_ratio_))
        xlabel = f'PC1 ({v[0]})'
        ylabel = f'PC2 ({v[1]})'
        zlabel = f'PC3 ({v[2]})'
        plt.subplot(4, 2, 2 * j + 1)
        for x, y, m, c in zip(xs, ys, ms, colors):
            plt.scatter(x, y, marker=m, c=c)
        # plt.xticks([])
        # plt.yticks([])
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.title(title + '(PC1, PC2)')
        plt.subplot(4, 2, 2 * j + 2)
        for x, y, m, c in zip(zs, ys, ms, colors):
            plt.scatter(x, y, marker=m, c=c)
        # plt.xticks([])
        # plt.yticks([])
        plt.xlabel(zlabel)
        plt.title(title + '(PC2, PC3)')
        
        if j == 0:
            plt.legend(handles=legend_elements, loc='upper right', ncol=2)
        plt.tight_layout()
        plt.savefig(os.path.join(output_folder, f'{prefix}3PCA.png'), dpi=200)

def postprocess_result(filename: str):
    df = dt.fread(filename).to_pandas()
    df = df.set_index(df.columns[0])
    df.index.name = 'gene'
    df = df.assign(gene_name=gene_name.loc[df.index],
                   gene_type=gene_type.loc[df.index])
    cols = list(df.columns)
    cols.insert(0, cols[-1])
    cols.insert(0, cols[-2])
    cols = cols[:-2]
    df = df[cols]
    df.to_csv(filename, sep='\t')

print("Plotting embeddings for non-adjusted counts...")
plot_embeddings(df, 'NA_')
print("Plotting embeddings for adjusted counts...")
plot_embeddings(df_adjusted, '')

print("Running edgeR [case-only]...")
for name, _ in d.items():
    print(name)
    out = os.path.join(output_folder, f'edger_{name}.tsv')
    subprocess.run(f'Rscript single_edge.r {adjusted_counts_filename} {name} {edger_hours} {edger_contrasts} {out} {edger_samples}'.split(' '))
    postprocess_result(out)

print("Running edgeR [case-control]...")
for group_b, group_a in combinations(d.keys(), 2):
    name = f'{group_a}_{group_b}'
    print(group_a, group_b)
    out = os.path.join(output_folder, f'edger_{name}.tsv')
    subprocess.run(f'Rscript edge.r {adjusted_counts_filename} {group_a} {group_b} {edger_hours} {edger_contrasts} {out} {edger_samples}'.split(' '))
    postprocess_result(out)

print("Running edgeR [case-control, 0h]...")
for group_b, group_a in combinations(d.keys(), 2):
    name = f'{group_a}_{group_b}'
    print(group_a, group_b)
    out = os.path.join(output_folder, f'edger_s_{name}.tsv')
    subprocess.run(f'Rscript edge.r {adjusted_counts_filename} {group_a} {group_b} 0 1 {out} {edger_samples}'.split(' '))
    postprocess_result(out)

print("Running edgeR [case-control, continuous time, intercepts in contrasts]...")
for group_b, group_a in combinations(d.keys(), 2):
    name = f'{group_a}_{group_b}'
    print(group_a, group_b)
    out = os.path.join(output_folder, f'edger_ct_int_{name}.tsv')
    subprocess.run(f'Rscript cont_time_edge.r {adjusted_counts_filename} {group_a} {group_b} 0;4;8 1 {out} {edger_samples}'.split(' '))
    postprocess_result(out)

print("Running edgeR [case-control, continuous time, intercepts zeroed in contrasts]...")
for group_b, group_a in combinations(d.keys(), 2):
    name = f'{group_a}_{group_b}'
    print(group_a, group_b)
    out = os.path.join(output_folder, f'edger_ct_{name}.tsv')
    subprocess.run(f'Rscript cont_time_edge.r {adjusted_counts_filename} {group_a} {group_b} 0;4;8 0 {out} {edger_samples}'.split(' '))
    postprocess_result(out)

print("Running edgeR [case-control, cubic polynomial]...")
for group_b, group_a in combinations(d.keys(), 2):
    name = f'{group_a}_{group_b}'
    print(group_a, group_b)
    out = 'edger_ct_nonlinear'
    subprocess.run(f'Rscript cont_time_poly_edge.r {adjusted_counts_filename} {group_a} {group_b} 0;4;8 {out} {edger_samples}'.split(' '))
    for file in filter(lambda x: x.startswith(out), os.listdir(output_folder)):
        postprocess_result(os.path.join(output_folder, file))



# print("Running ImpulseDE2 [case-only]...")
# for name, its in d.items():
#     print(name)
#     tmp_filename = os.path.join(tmp_folder, f'tmp_annot_{name}.tsv')
#     out = os.path.join(output_folder, f'ImpulseDE2_{name}.tsv')
#     lt = list()
#     for name, t, c in its:
#         lt.append((name, 'case', t, "B"))
#     pd.DataFrame(lt, columns=['Sample', 'Condition','Time',
#                               'Batch']).to_csv(tmp_filename, index=False, sep='\t')
#     subprocess.run(f'Rscript impulse.r {adjusted_counts_filename} {tmp_filename} {out}'.split(' '))
#     postprocess_result(out)
#     os.remove(tmp_filename)

# print("Running ImpulseDE2 [case-control]...")
# for group_b, group_a in combinations(d.keys(), 2):
#     its_a = d[group_a]
#     its_b = d[group_b]
#     name = f'{group_a}_{group_b}'
#     print(group_a, group_b)
#     tmp_filename = os.path.join(tmp_folder, f'tmp_annot_{name}.tsv')
#     out = os.path.join(output_folder, f'ImpulseDE2_{name}.tsv')
#     lt = list()
#     for name, t, c in its_a:
#         lt.append((name, 'case', t, "B"))
#     for name, t, c in its_b:
#         lt.append((name, 'control', t, "B"))
#     pd.DataFrame(lt, columns=['Sample', 'Condition','Time',
#                               'Batch']).to_csv(tmp_filename, index=False, sep='\t')
#     subprocess.run(f'Rscript impulse.r {adjusted_counts_filename} {tmp_filename} {out} 1'.split(' '))
#     postprocess_result(out)
#     os.remove(tmp_filename)

print("Building correlation matrix...")
print("Warning: Correlation-building script is hardcoded to work with some"
      " pre-defined seetings. If they are changed, results are not guaranteed"
      " to align well with new settings.")
subprocess.run('Rscript corr.r'.split(' '), stdout=open(os.devnull, 'wb'),
               stderr=open(os.devnull, 'wb'))

os.removedirs(tmp_folder)

print("Building extra plots...")
folder = os.path.join('results', 'barplots')
os.makedirs(folder, exist_ok=True)
tdf = df_adjusted.copy()
tdfn = normalize(tdf)
# tdf = cpm(df_adjusted.copy(), norm_factors='TMM')
for a, b in combinations(d.keys(), 2):
    legend_elements = [plt.scatter([0], [0], color='b', marker='s', label=a),
                       plt.scatter([0], [0], color='r', marker='s', label=b)
                       ]
    
    first = next(iter(d[a]))[1]
    plt.figure(figsize=(14, 12), dpi=200)
    plt.subplot(2, 1, 1)
    hd = {first}
    subdf = pd.DataFrame()
    colors = []
    for (acol, ah, _), (bcol, bh, _) in zip(d[a], d[b]):
        curlen = len(hd)
        hd.add(ah)
        newlen = len(hd)
        at = tdf[[acol]]
        at.columns = ['_'.join(acol.split('_')[-2:])]
        bt = tdf[[bcol]]
        bt.columns = ['_'.join(bcol.split('_')[-2:])]
        if newlen > curlen:
            t = at.copy()
            t.columns = [str()]
            t.values[:] = 0
            subdf = pd.concat([subdf, t, at, bt], axis=1)
            colors.extend(['black', 'blue','red'])
        else:
            subdf = pd.concat([subdf, at, bt], axis=1)
            colors.extend(['blue', 'red'])
    ax = sns.barplot(data=subdf.values, palette=colors)
    ax.set_xticklabels(subdf.columns, rotation=50)
    plt.ylabel('Counts')
    plt.legend(handles=legend_elements, loc='upper right',)
    plt.title('No normalization')
    plt.subplot(2, 1, 2)
    hd = {first}
    subdf = pd.DataFrame()
    colors = []
    for (acol, ah, _), (bcol, bh, _) in zip(d[a], d[b]):
        curlen = len(hd)
        hd.add(ah)
        newlen = len(hd)
        at = tdfn[[acol]]
        at.columns = ['_'.join(acol.split('_')[-2:])]
        bt = tdfn[[bcol]]
        bt.columns = ['_'.join(bcol.split('_')[-2:])]
        if newlen > curlen:
            t = at.copy()
            t.columns = [str()]
            t.values[:] = 0
            subdf = pd.concat([subdf, t, at, bt], axis=1)
            colors.extend(['black', 'blue','red'])
        else:
            subdf = pd.concat([subdf, at, bt], axis=1)
            colors.extend(['blue', 'red'])
    ax = sns.barplot(data=subdf.values, palette=colors)
    ax.set_xticklabels(subdf.columns, rotation=50)
    plt.ylabel('Normalized counts')
    # plt.legend(handles=legend_elements, loc='upper right',)
    plt.title('TMM+CPM normalized')
    plt.tight_layout()
    plt.savefig(os.path.join(folder, f'{a}-vs-{b}.png'))

colors_mapping = {'dd': 'black',
                  'dd_pYB1': 'orange',
                  'dd_pYB3': 'green',
                  'dd_pYB1_pYB3': 'red'}
legend_elements = [plt.scatter([0], [0],
                               color=c, marker='s', label=k)
                   for k, c in colors_mapping.items()]
plt.figure(figsize=(20, 12), dpi=200)
colors = list()
subdf = pd.DataFrame()
hd = {first}
subdf = pd.DataFrame()
colors = []
keys = list(colors_mapping.keys())
cs = [colors_mapping[k] for k in keys]
plt.subplot(2, 1, 1)
for its in zip(*[d[a] for a in keys]):
    curlen = len(hd)
    h = its[0][1]
    hd.add(h)
    newlen = len(hd)
    ndf = pd.DataFrame()
    for (col, _, _) in its:
        at = tdf[[col]]
        at.columns = ['_'.join(col.split('_')[-2:])]
        ndf = pd.concat([ndf, at], axis=1)
    t = at.copy()
    t.columns = [str()]
    t.values[:] = 0
    if newlen > curlen:
        ndf = pd.concat([t, ndf], axis=1)
        colors.append('white')
    if not subdf.empty:
        ndf = pd.concat([t, ndf], axis=1)
        colors.append('white')
    colors.extend(cs)
    subdf = pd.concat([subdf, ndf], axis=1)
ax = sns.barplot(data=subdf.values, palette=colors)
ax.set_xticklabels(subdf.columns, rotation=50)
plt.ylabel('Counts')
plt.legend(handles=legend_elements, loc='upper right', ncol=2)
plt.title('No normalization')
plt.subplot(2, 1, 2)
subdf = pd.DataFrame()
colors = list()
hd = set()
for its in zip(*[d[a] for a in keys]):
    curlen = len(hd)
    h = its[0][1]
    hd.add(h)
    newlen = len(hd)
    ndf = pd.DataFrame()
    for (col, _, _) in its:
        at = tdfn[[col]]
        at.columns = ['_'.join(col.split('_')[-2:])]
        ndf = pd.concat([ndf, at], axis=1)
    t = at.copy()
    t.columns = [str()]
    t.values[:] = 0
    if newlen > curlen:
        ndf = pd.concat([t, ndf], axis=1)
        colors.append('white')
    if not subdf.empty:
        ndf = pd.concat([t, ndf], axis=1)
        colors.append('white')
    colors.extend(cs)
    subdf = pd.concat([subdf, ndf], axis=1)
ax = sns.barplot(data=subdf.values, palette=colors)
ax.set_xticklabels(subdf.columns, rotation=50)
plt.ylabel('Counts')
# plt.legend(handles=legend_elements, loc='upper right', ncol=2)
plt.title('TMM+CPM normalization')
plt.savefig(os.path.join(folder, 'all.png'))


tdf = tdf.loc[test_gene_counts_indices].copy()
tdfn = tdfn.loc[test_gene_counts_indices].copy()
tdf.index = test_gene_counts
tdfn.index = test_gene_counts
subdf = pd.DataFrame()
colors = []
keys = list(colors_mapping.keys())
cs = ['blue', 'red', 'green', 'orange', 'yellow']
legend_elements = [plt.scatter([0], [0],
                   color=c, marker='s', label=k)
                   for k, c in zip(test_gene_counts, cs)]
plt.figure(figsize=(20, 20), dpi=200)
plt.subplot(2, 1, 1)
for its in zip(*[d[a] for a in keys]):
    ndf = pd.DataFrame()
    for (col, _, _) in its:
        at = tdf[[col]]
        ndf = pd.concat([ndf, at], axis=1)
    if not subdf.empty:
        t = at.copy()
        t.columns = [str()]
        t.values[:] = 0
        ndf = pd.concat([t, ndf], axis=1)
    colors.extend(cs[:len(test_gene_counts)] * len(its))
    subdf = pd.concat([subdf, ndf], axis=1)
x = list(range(subdf.shape[1]))
for k, c in zip(test_gene_counts, cs):
    ax = sns.barplot(x=x,
                     y=subdf.loc[[k]].values.flatten(), color=c, alpha=0.8)
labels = list()
ticks = list()
for t, l in zip(x, subdf.columns):
    if l:
        ticks.append(t)
        labels.append(l)
plt.xticks(ticks, labels, rotation=65)
plt.ylabel('Counts')
plt.legend(handles=legend_elements, loc='upper right')
plt.title('No normalization')
subdf = pd.DataFrame()
colors = []
keys = list(colors_mapping.keys())
cs = ['blue', 'red', 'green', 'orange', 'yellow']
plt.subplot(2, 1, 2)
for its in zip(*[d[a] for a in keys]):
    ndf = pd.DataFrame()
    for (col, _, _) in its:
        at = tdfn[[col]]
        ndf = pd.concat([ndf, at], axis=1)
    if not subdf.empty:
        t = at.copy()
        t.columns = [str()]
        t.values[:] = 0
        ndf = pd.concat([t, ndf], axis=1)
    colors.extend(cs[:len(test_gene_counts)] * len(its))
    subdf = pd.concat([subdf, ndf], axis=1)
x = list(range(subdf.shape[1]))
for k, c in zip(test_gene_counts, cs):
    ax = sns.barplot(x=x,
                     y=subdf.loc[[k]].values.flatten(), color=c, alpha=0.8)
labels = list()
ticks = list()
for t, l in zip(x, subdf.columns):
    if l:
        ticks.append(t)
        labels.append(l)
plt.xticks(ticks, labels, rotation=65)
plt.ylabel('Counts')
plt.title('TMM+CPM-normalized counts')
plt.tight_layout()
plt.savefig(os.path.join(folder, 'test_genes.png'))



plt.figure(figsize=(8 * len(test_gene_counts), 8), dpi=200)
for i, gene in enumerate(test_gene_counts):
    counts = list()
    for line in keys:
        c = 0
        for col, _, _ in d[line]:
            c += tdfn.loc[gene, col]
        counts.append(c)
    plt.subplot(1, len(test_gene_counts), i + 1)
    plt.pie(counts, labels=keys, autopct='%.2f%%')
    plt.title(gene)
plt.suptitle("YBX-genes normalized count distribution across cell lines")
plt.tight_layout()
plt.savefig(os.path.join(output_folder, 'pie_ybx_counts.png'))


f = plt.figure(figsize=(8 * len(test_gene_counts), 8 * len(d)), dpi=200,)
subf = f.subfigures(nrows=len(d), ncols=1)
for (j, (line, cols)), sf in zip(enumerate(d.items()), subf):
    axs = sf.subplots(nrows=1, ncols=len(test_gene_counts))
    batches_dict = defaultdict(list)
    cols = [c[0] for c in cols]
    for c, b in zip(df.columns, batches):
        if c in cols:
            batches_dict[b].append(c)
    for i, gene in enumerate(test_gene_counts):
        counts = list()
        for batch in sorted(set(batches_dict)):
            c = 0
            for col in batches_dict[batch]:
                c += tdfn.loc[gene, col]
            counts.append(c / len(batches_dict[batch]))
        axs[i].pie(counts, labels=sorted(set(batches_dict)), autopct='%.2f%%')
        axs[i].set_title(gene)
    sf.suptitle(f"YBX-genes normalized count distribution across batches for {line} cell line")
plt.tight_layout()
plt.savefig(os.path.join(output_folder, 'pie_ybx_counts_batches.png'))

print("Building plots for differential expression...")
folder = os.path.join(output_folder, 'de_figs')
os.makedirs(folder, exist_ok=True)

plt.figure(figsize=(14, 14), dpi=200)
for i, line in enumerate(d):
    de = dt.fread(os.path.join(output_folder, f'edger_{line}.tsv')).to_pandas()
    prc = list()
    counts = list()
    x = list()
    cur = 0.0000005; end = 0.05
    while cur <= end:
        inds = de.FDR < cur
        counts.append(sum(inds))
        prc.append(sum(de[inds].logFC < 0) / counts[-1])
        x.append(cur)
        cur *= 10
    plt.subplot(2, int(np.ceil(len(d) / 2)), i + 1)
    x = -np.log10(x)
    plt.plot(x, prc)
    labels = [f'5e-{int(np.ceil(x))}' for x in x]
    for xp, y, c in zip(x, prc, counts):
        plt.text(xp -0.25, y + 0.008, str(c))
        plt.plot(xp, y, 'kx')
    plt.ylim(0.45, 1.05)
    plt.xticks(x, labels, rotation=50)
    plt.xlabel('FDR')
    if i % 2 == 0:
        plt.ylabel('Fraction of logFC < 0')
    else:
        plt.yticks([])
    plt.title(line)
plt.tight_layout()
plt.savefig(os.path.join(folder, f'edger_co.png'))

plt.figure(figsize=(14, 14), dpi=200)
for i, line in enumerate(d):
    dee = dt.fread(os.path.join(output_folder, f'edger_{line}.tsv')).to_pandas()
    dee = dee.set_index(dee.columns[0])
    de = dt.fread(os.path.join(output_folder, f'ImpulseDE2_{line}.tsv')).to_pandas()
    de = de.set_index(de.columns[0])
    prc = list()
    counts = list()
    x = list()
    cur = 0.0000000000000005; end = 0.05
    while cur <= end:
        inds = de.padj < cur
        inds = inds[dee.index]
        counts.append(sum(inds))
        prc.append(sum(dee[inds].logFC < 0) / counts[-1])
        x.append(cur)
        cur *= 10
    plt.subplot(2, int(np.ceil(len(d) / 2)), i + 1)
    x = -np.log10(x)
    plt.plot(x, prc)
    labels = [f'5e-{int(np.ceil(x))}' for x in x]
    for xp, y, c in zip(x, prc, counts):
        plt.text(xp -0.25, y + 0.008, str(c))
        plt.plot(xp, y, 'kx')
    plt.ylim(0.45, 1.05)
    plt.xticks(x, labels, rotation=50)
    plt.xlabel('FDR')
    if i % 2 == 0:
        plt.ylabel('Fraction of logFC < 0')
    else:
        plt.yticks([])
    plt.title(line)
plt.tight_layout()
plt.savefig(os.path.join(folder, f'ImpulseDE2_co.png'))


for i, line in enumerate(d):
    de = dt.fread(os.path.join(output_folder, f'edger_{line}.tsv')).to_pandas()
    prc = list()
    counts = list()
    x = list()
    cur = 0.05
    while True:
        inds = de.FDR < cur
        counts.append(sum(inds))
        if counts[-1] == 0:
            del counts[-1]
            break
        prc.append(sum(de[inds].logFC < 0) / counts[-1])
        x.append(cur)
        cur /= 10
    x = x[::-1]
    prc = prc[::-1]
    counts = counts[::-1]
    x = -np.log10(x)
    plt.figure(figsize=(8, 8))
    plt.plot(x, prc)
    labels = [f'5e-{int(np.ceil(x))}' for x in x]
    for xp, y, c in zip(x, prc, counts):
        plt.text(xp -0.25, y + 0.008, str(c))
        plt.plot(xp, y, 'kx')
    plt.xticks(x, labels, rotation=50)
    plt.xlabel('FDR')
    plt.ylabel('Fraction of logFC < 0')
    plt.title(line)
    plt.tight_layout()
    plt.savefig(os.path.join(folder, f'edger_co_{line}.png'))
    

plt.figure(figsize=(10, 16))
for i, (b, a) in enumerate(combinations(d.keys(), 2)):
    de = dt.fread(os.path.join(output_folder, f'edger_{a}_{b}.tsv')).to_pandas()
    prc = list()
    counts = list()
    x = list()
    cur = 0.05
    while True:
        inds = de.FDR < cur
        counts.append(sum(inds))
        if counts[-1] == 0:
            del counts[-1]
            break
        prc.append(sum(de[inds].logFC < 0) / counts[-1])
        x.append(cur)
        cur /= 10
    x = x[::-1]
    prc = prc[::-1]
    counts = counts[::-1]
    x = -np.log10(x)
    plt.subplot(int(np.ceil(len(list(combinations(d.keys(), 2))) / 2)), 2,
                i + 1)
    plt.plot(x, prc)
    labels = [f'5e-{int(np.ceil(x))}' for x in x]
    for xp, y, c in zip(x, prc, counts):
        plt.text(xp -0.25, y + 0.008, str(c))
        plt.plot(xp, y, 'kx')
    plt.ylim(-0.05, 1.05)
    plt.xticks(x, labels, rotation=50)
    plt.xlabel('FDR')
    if i % 2 == 0:
        plt.ylabel('Fraction of logFC < 0')
    else:
        plt.yticks([])
    plt.title(f'{a}[case] vs {b}[control]')
plt.tight_layout()
plt.savefig(os.path.join(folder, f'edger_case_control.png'))

plt.figure(figsize=(10, 16))
for i, (b, a) in enumerate(combinations(d.keys(), 2)):
    de = dt.fread(os.path.join(output_folder, f'edger_ct_{a}_{b}.tsv')).to_pandas()
    prc = list()
    counts = list()
    x = list()
    cur = 0.05
    while True:
        inds = de.FDR < cur
        counts.append(sum(inds))
        if counts[-1] == 0:
            del counts[-1]
            break
        prc.append(sum(de[inds].logFC < 0) / counts[-1])
        x.append(cur)
        cur /= 10
    x = x[::-1]
    prc = prc[::-1]
    counts = counts[::-1]
    x = -np.log10(x)
    plt.subplot(int(np.ceil(len(list(combinations(d.keys(), 2))) / 2)), 2,
                i + 1)
    plt.plot(x, prc)
    labels = [f'5e-{int(np.ceil(x))}' for x in x]
    for xp, y, c in zip(x, prc, counts):
        plt.text(xp -0.25, y + 0.008, str(c))
        plt.plot(xp, y, 'kx')
    plt.ylim(-0.05, 1.05)
    plt.xticks(x, labels, rotation=50)
    plt.xlabel('FDR')
    if i % 2 == 0:
        plt.ylabel('Fraction of logFC < 0')
    else:
        plt.yticks([])
    plt.title(f'{a}[case] vs {b}[control]')
plt.tight_layout()
plt.savefig(os.path.join(folder, f'edger_ct_case_control.png'))

plt.figure(figsize=(10, 16))
for i, (b, a) in enumerate(combinations(d.keys(), 2)):
    de = dt.fread(os.path.join(output_folder, f'edger_ct_int_{a}_{b}.tsv')).to_pandas()
    prc = list()
    counts = list()
    x = list()
    cur = 0.05
    while True:
        inds = de.FDR < cur
        counts.append(sum(inds))
        if counts[-1] == 0:
            del counts[-1]
            break
        prc.append(sum(de[inds].logFC < 0) / counts[-1])
        x.append(cur)
        cur /= 10
    x = x[::-1]
    prc = prc[::-1]
    counts = counts[::-1]
    x = -np.log10(x)
    plt.subplot(int(np.ceil(len(list(combinations(d.keys(), 2))) / 2)), 2,
                i + 1)
    plt.plot(x, prc)
    labels = [f'5e-{int(np.ceil(x))}' for x in x]
    for xp, y, c in zip(x, prc, counts):
        plt.text(xp -0.25, y + 0.008, str(c))
        plt.plot(xp, y, 'kx')
    plt.ylim(-0.05, 1.05)
    plt.xticks(x, labels, rotation=50)
    plt.xlabel('FDR')
    if i % 2 == 0:
        plt.ylabel('Fraction of logFC < 0')
    else:
        plt.yticks([])
    plt.title(f'{a}[case] vs {b}[control]')
plt.tight_layout()
plt.savefig(os.path.join(folder, f'edger_ct_int_case_control.png'))


plt.figure(figsize=(10, 16))
for i, (b, a) in enumerate(combinations(d.keys(), 2)):
    dee = dt.fread(os.path.join(output_folder, f'edger_{a}_{b}.tsv')).to_pandas()
    dee = dee.set_index(dee.columns[0])
    de = dt.fread(os.path.join(output_folder, f'ImpulseDE2_{a}_{b}.tsv')).to_pandas()
    de = de.set_index(de.columns[0])
    prc = list()
    counts = list()
    x = list()
    cur = 0.05
    while True:
        inds = de.padj < cur
        inds = inds[dee.index]
        counts.append(sum(inds))
        if counts[-1] == 0:
            del counts[-1]
            break
        prc.append(sum(dee[inds].logFC < 0) / counts[-1])
        x.append(cur)
        cur /= 1000
        if cur < 1e-30:
            break
    x = x[::-1]
    prc = prc[::-1]
    counts = counts[::-1]
    x = -np.log10(x)
    plt.subplot(int(np.ceil(len(list(combinations(d.keys(), 2))) / 2)), 2,
                i + 1)
    plt.plot(x, prc)
    labels = [f'5e-{int(np.ceil(x))}' for x in x]
    for xp, y, c in zip(x, prc, counts):
        plt.text(xp -0.25, y + 0.008, str(c))
        plt.plot(xp, y, 'kx')
    plt.ylim(-0.05, 1.05)
    plt.xticks(x, labels, rotation=50)
    plt.xlabel('FDR')
    if i % 2 == 0:
        plt.ylabel('Fraction of logFC < 0')
    else:
        plt.yticks([])
    plt.title(f'{a}[case] vs {b}[control]')
plt.tight_layout()
plt.savefig(os.path.join(folder, f'ImpulseDE2_case_control.png'))

plt.figure(figsize=(12, 16))
for i, (b, a) in enumerate(combinations(d.keys(), 2)):
    de = dt.fread(os.path.join(output_folder, f'edger_s_{a}_{b}.tsv')).to_pandas()
    prc = list()
    counts = list()
    x = list()
    cur = 0.05
    while True:
        inds = de.FDR < cur
        counts.append(sum(inds))
        if counts[-1] == 0:
            del counts[-1]
            break
        prc.append(sum(de[inds].logFC < 0) / counts[-1])
        x.append(cur)
        cur /= 10
    x = x[::-1]
    prc = prc[::-1]
    counts = counts[::-1]
    x = -np.log10(x)
    plt.subplot(int(np.ceil(len(list(combinations(d.keys(), 2))) / 2)), 2,
                i + 1)
    plt.plot(x, prc)
    labels = [f'5e-{int(np.ceil(x))}' for x in x]
    for xp, y, c in zip(x, prc, counts):
        plt.text(xp -0.25, y + 0.008, str(c))
        plt.plot(xp, y, 'kx')
    plt.ylim(-0.05, 1.05)
    plt.xticks(x, labels, rotation=50)
    plt.xlabel('FDR')
    if i % 2 == 0:
        plt.ylabel('Fraction of logFC < 0')
    else:
        plt.yticks([])
    plt.title(f'{a}[case] vs {b}[control]')
    plt.tight_layout()
    plt.savefig(os.path.join(folder, f'edger_h0_case_control.png'))




print("Building intersection matrix...")
edger_gs = defaultdict(set)
impulse_gs = defaultdict(set)
columns = list()
multi_cols = list()
latex = os.path.join(output_folder, 'latex_tables')
os.makedirs(latex, exist_ok=True)
for name in d:
    filename = os.path.join(output_folder, f'ImpulseDE2_{name}.tsv')
    r = dt.fread(filename).to_pandas()
    r = r.set_index(r.columns[0])['padj']
    r = set(r.index[r < pval_cutoff])
    impulse_gs[name] = r
    columns.append(f'ImpulseDE2.{name}')
    multi_cols.append(('Case-only', name, 'ImpulseDE'))
    filename = os.path.join(output_folder, f'edger_{name}.tsv')
    r = dt.fread(filename).to_pandas()
    r = r.set_index(r.columns[0])['FDR']
    r = set(r.index[r < pval_cutoff])
    edger_gs[name] = r
    columns.append(f'edgeR.{name}')
    multi_cols.append(('Case-only', name, 'edgeR'))
for b, a in combinations(d.keys(), 2):
    name = f'{a}_{b}'
    filename = os.path.join(output_folder, f'ImpulseDE2_{name}.tsv')
    r = dt.fread(filename).to_pandas()
    r = r.set_index(r.columns[0])['padj']
    r = set(r.index[r < pval_cutoff])
    impulse_gs[name] = r
    columns.append(f'ImpulseDE2.{name}')
    multi_cols.append(('Case-control', f'{a}-{b}', 'ImpulseDE'))
    filename = os.path.join(output_folder, f'edger_{name}.tsv')
    r = dt.fread(filename).to_pandas()
    r = r.set_index(r.columns[0])['FDR']
    r = set(r.index[r < pval_cutoff])
    edger_gs[name] = r
    columns.append(f'edgeR.{name}')
    multi_cols.append(('Case-control', f'{a}-{b}', 'edgeR'))
multi_cols = pd.MultiIndex.from_tuples(multi_cols)
n = len(columns)
mx = np.zeros((n, n))
for i, j in np.nditer(np.triu_indices(n)):
    a = columns[i]; b = columns[j]
    print(a, b)
    a_m, a = a.split('.');  b_m, b = b.split('.')
    if a_m == 'ImpulseDE2':
        a = impulse_gs[a]
    else:
        a = edger_gs[a]
    if b_m == 'ImpulseDE2':
        b = impulse_gs[b]
    else:
        b = edger_gs[b]
    c = a & b
    mx[i, j] = len(c)
    mx[j, i] = mx[i, j]



mx = pd.DataFrame(mx, columns=multi_cols, index=multi_cols)
mx.to_csv(os.path.join(output_folder, "intersection_methods.tsv"), sep='\t')
mx.values[np.tril_indices_from(mx, -1)] = np.nan
r = mx.to_latex(multirow=True,float_format="%.0f", multicolumn_format='c')
r = r.replace('NaN', str())
with open(os.path.join(latex, 'im_full.txt'), 'w') as f:
    f.write(r)
c = list(filter(lambda x: x[-1] == 'ImpulseDE', mx.columns))
r = mx.loc[c, c].to_latex(multirow=True, float_format="%.0f", multicolumn_format='c')
r = r.replace('NaN', str())
with open(os.path.join(latex, 'im_impulse.txt'), 'w') as f:
    f.write(r)
c = list(filter(lambda x: x[-1] == 'edgeR', mx.columns))
r = mx.loc[c, c].to_latex(multirow=True, float_format="%.0f", multicolumn_format='c')
r = r.replace('NaN', str())
with open(os.path.join(latex, 'im_edger.txt'), 'w') as f:
    f.write(r)
c = list(filter(lambda x: x[0] == 'Case-only', mx.columns))
r = mx.loc[c, c].to_latex(multirow=True, float_format="%.2f", multicolumn_format='c')
r = r.replace('NaN', str())
with open(os.path.join(latex, 'im_co.txt'), 'w') as f:
    f.write(r)
c = list(filter(lambda x: x[0] == 'Case-control', mx.columns))
r = mx.loc[c, c].to_latex(multirow=True, float_format="%.2f", multicolumn_format='c')
r = r.replace('NaN', str())
with open(os.path.join(latex, 'im_cc.txt'), 'w') as f:
    f.write(r)


mx = np.zeros((n, n))
for i, j in np.nditer(np.triu_indices(n)):
    a = columns[i]; b = columns[j]
    print(a, b)
    a_m, a = a.split('.');  b_m, b = b.split('.')
    if a_m == 'ImpulseDE2':
        a = impulse_gs[a]
    else:
        a = edger_gs[a]
    if b_m == 'ImpulseDE2':
        b = impulse_gs[b]
    else:
        b = edger_gs[b]
    c = a & b
    if len(a) * len(b) == 0:
        mx[i, j] = np.nan
    else:
        mx[i, j] = len(c) / len(a | b) * 100
    if i == j:
        mx[i, j] = len(a) / df.shape[0] * 100
    mx[j, i] = mx[i, j]
mx = pd.DataFrame(mx, columns=multi_cols, index=multi_cols)
mx.to_csv(os.path.join(output_folder, "intersection_methods_perc.tsv"), sep='\t')
mx.values[np.tril_indices_from(mx, -1)] = np.nan
r = mx.to_latex(multirow=True, float_format="%.2f", multicolumn_format='c')
r = r.replace('NaN', str())
with open(os.path.join(latex, 'imj_full.txt'), 'w') as f:
    f.write(r)
c = list(filter(lambda x: x[0] == 'Case-only', mx.columns))
r = mx.loc[c, c].to_latex(multirow=True, float_format="%.2f", multicolumn_format='c')
r = r.replace('NaN', str())
with open(os.path.join(latex, 'imj_co.txt'), 'w') as f:
    f.write(r)
c = list(filter(lambda x: x[0] == 'Case-control', mx.columns))
r = mx.loc[c, c].to_latex(multirow=True, float_format="%.2f", multicolumn_format='c')
r = r.replace('NaN', str())
with open(os.path.join(latex, 'imj_cc.txt'), 'w') as f:
    f.write(r)
c = list(filter(lambda x: x[-1] == 'ImpulseDE', mx.columns))
r = mx.loc[c, c].to_latex(multirow=True, float_format="%.2f", multicolumn_format='c')
r = r.replace('NaN', str())
with open(os.path.join(latex, 'imj_impulse.txt'), 'w') as f:
    f.write(r)
c = list(filter(lambda x: x[-1] == 'edgeR', mx.columns))
r = mx.loc[c, c].to_latex(multirow=True, float_format="%.2f", multicolumn_format='c')
r = r.replace('NaN', str())
with open(os.path.join(latex, 'imj_edger.txt'), 'w') as f:
    f.write(r)

import funclust