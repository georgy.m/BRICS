library(ImpulseDE2)
args = commandArgs(trailingOnly=TRUE)
if (length(args) == 0)
{
  args = c('results/adjusted_counts.tsv', 'tmp/tmp_annot_dd.tsv', 'results/ImpulseDE2_dd.tsv', '0')
}
if (length(args) == 4)
{
  caseCtrl = as.logical(as.integer(args[4]))
}else
{
  caseCtrl = FALSE
}
print("Arguments:"); print(cat(args, sep="|"))
counts = read.table(args[1], row.names=1, sep='\t', header=T)
annot = read.table(args[2], sep='\t', header=T)
annot$Time = round(annot$Time)
cnt = counts[annot$Sample]
re = runImpulseDE2(round(as.matrix(cnt)), annot, boolCaseCtrl=caseCtrl,
                    boolIdentifyTransients=TRUE, scaNProc=6)
table = re$dfImpulseDE2Results
fits = re@lsModelFits$case

imp_beta = c(); imp_h0 = c(); imp_h1 = c(); imp_h2 = c(); imp_t1 = c(); imp_t2 = c()
sig_beta = c(); sig_h0 = c(); sig_h1 = c(); sig_t = c()
for (fit in fits)
{
 imp = fit$lsImpulseFit$vecImpulseParam
 imp_beta = c(imp_beta, as.double(imp['beta']))
 imp_h0 = c(imp_h0, as.double(imp['h0']))
 imp_h1 = c(imp_h1, as.double(imp['h1']))
 imp_h2 = c(imp_h2, as.double(imp['h2']))
 imp_t1 = c(imp_t1, as.double(imp['t1']))
 imp_t2 = c(imp_t2, as.double(imp['t2']))
 sig = fit$lsSigmoidFit$vecSigmoidParam
 sig_beta = c(sig_beta, as.double(sig['beta']))
 sig_h0 = c(sig_h0, as.double(sig['h0']))
 sig_h1 = c(sig_h1, as.double(sig['h1']))
 sig_t = c(sig_t, as.double(sig['t']))
}
table[, 'imp_beta'] = imp_beta; table[, 'imp_h0'] = imp_h0; table[, 'imp_h1'] = imp_h1
table[, 'imp_h2'] = imp_h2; table[, 'imp_t1'] = imp_t1; table[, 'imp_t2'] = imp_t2

table[, 'sig_beta'] = sig_beta; table[, 'sig_h0'] = sig_h0; table[, 'sig_h1'] = sig_h1
table[, 'sig_t'] = sig_t
write.table(table, args[3], sep='\t', row.names=F)
# 
# library(ggplot2)
# lsgplotsGenes <- plotGenes(
#   vecGeneIDs       = NULL,
#   scaNTopIDs       = 10,
#   objectImpulseDE2 = re,
#   boolCaseCtrl     = FALSE,
#   dirOut           = 'results',
#   strFileName      = NULL,
#   vecRefPval       = NULL, 
#   strNameRefMethod = NULL)
# print(lsgplotsGenes[[1]])
# 
# library(ComplexHeatmap)
# lsHeatmaps <- plotHeatmap(
#   objectImpulseDE2       = re,
#   strCondition           = "case",
#   boolIdentifyTransients = TRUE,
#   scaQThres              = 0.001)
# draw(lsHeatmaps$complexHeatmapRaw) # Heatmap based on normalised counts