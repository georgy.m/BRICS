#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  3 08:15:42 2021

@author: georgy
"""

import jax.numpy as jnp
import datatable as dt
import pandas as pd
import numpy as np
from conorm import cpm, tmm_norm_factors, tmm
from collections import defaultdict
from jax import jit, grad
from jax.scipy.optimize import minimize
from jax.scipy.special import logsumexp
from jax.config import config
config.update("jax_enable_x64", True)

df = dt.fread('BRIC_gene_counts_wgt.tsv', sep='\t').to_pandas()
df = df.set_index('gene')
if 'gene_name' in df.columns:
    gene_name = df['gene_name']
    gene_type = df['gene_type']
filename = 'results/adjusted_counts.tsv'
df = dt.fread(filename).to_pandas().set_index('Gene')
df = df.loc[np.all(df != 0, axis=1)]
hs = set(dt.fread('hs.tsv').to_pandas().values.flatten())
# df = cpm(df, norm_factors='TMM')
hours = ['0h', '4h', '8h']
lines = set()
cols = list(df.columns)
dcols = defaultdict(lambda: defaultdict(list))
ncols = list()
for c in cols:
    its = c.split('_')
    h = its[-2]
    if h not in hours:
        continue
    line = '_'.join(its[:-2])
    dcols[line][h].append(c)
    ncols.append(c)
tdf, nf = tmm(df[ncols], return_norm_factors=True)
tdf = cpm(df[ncols], norm_factors=nf)

def is_monotone(df: pd.DataFrame, cols: list):
    lt = [df[col].T.mean() for col in cols]
    res = np.ones(len(df), dtype=bool)
    for i in range(len(lt) - 1):
        res &= lt[i] >= lt[i + 1]
    return pd.Series(res, index=df.index)

norm_gene = 'ENSG00000109971.14'
for line, hours in dcols.items():
    base_count = tdf.loc[norm_gene, hours['0h']].mean()
    for h, lt in hours.items():
        if h == '0h':
            continue
        for col in lt:
            cnt = tdf.loc[norm_gene, col]
            correction = cnt / base_count
            nf.loc[col] *= correction
            # tdf[col] /= correction
nf /= np.prod(nf)**(1/len(nf))
def postprocess_result(filename: str):
    df = dt.fread(filename).to_pandas()
    df = df.set_index(df.columns[0])
    df.index.name = 'gene'
    df = df.assign(gene_name=gene_name.loc[df.index],
                   gene_type=gene_type.loc[df.index])
    cols = list(df.columns)
    cols.insert(0, cols[-1])
    cols.insert(0, cols[-2])
    cols = cols[:-2]
    df = df[cols]
    df.to_csv(filename, sep='\t')

df = cpm(df, norm_factors=nf)
nf.to_csv('results/norm_factors.tsv', sep='\t')
df.to_csv('results/normalized_adjusted_counts.tsv', sep='\t')
postprocess_result('results/normalized_adjusted_counts.tsv')