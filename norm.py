import datatable as dt
import pandas as pd 
import numpy as np
import semopy
import semba
import matplotlib.pyplot as plt
from conorm import cpm
from collections import defaultdict
from scipy.stats import linregress
from itertools import combinations
from statsmodels.stats.multitest import multipletests
from scipy.stats import gmean
from random import shuffle
from time import time
import statsmodels.api as sm

mp_correction = True
filename = 'results/adjusted_counts.tsv'
df = dt.fread(filename).to_pandas().set_index('Gene')
hs = set(dt.fread('hs.tsv').to_pandas().values.flatten())
# df = cpm(df, norm_factors='TMM')
hours = ['0h', '4h', '8h']
lines = set()
cols = list(df.columns)
dcols = defaultdict(lambda: defaultdict(list))
ncols = list()
for c in cols:
    its = c.split('_')
    h = its[-2]
    if h not in hours:
        continue
    line = '_'.join(its[:-2])
    dcols[line][h].append(c)
    ncols.append(c)

df = df[ncols]
tdf = dt.fread('BRIC_gene_counts_wgt.tsv').to_pandas().set_index('gene')
gene_name = tdf['gene_name'].loc[df.index]
del tdf
hk = [g in hs for g in gene_name.values]
all_df = df.copy()
# df = df.loc[hk]
gene_names = dict()
for g, n in gene_name.iteritems():
    gene_names[g] = n

def test_monotonicity(df: pd.DataFrame, cols: list, bayes=False):
    desc = '''h4 ~ 1.0*h0
    h8 ~ 1.0 * h4'''
    genes = dict()
    inds = list()
    n = 0
    all_cols = list()
    for cs in cols:
        all_cols.extend(cs)
    df = cpm(df[all_cols], norm_factors='TMM')
    for cs in cols:
        inds.append(list(map(all_cols.index, cs)))
    gs = list(df.index)
    df = df.T
    df -= df.mean()
    df /= df.std()
    df = df.T
    mx = df.values
    repl = pd.DataFrame(np.zeros((3, 3)), columns=['h0', 'h4', 'h8'])
    t_inds = np.zeros(6)
    t_inds[1::2] = 1
    for i in range(len(mx)):
        if not i % 200:
            print(i / len(mx) * 100)
        for j, ind in enumerate(inds):
            repl.values[:, j] = mx[i, ind]
        if bayes:
            m = semba.ModelMeans(desc, intercepts=True)
        else:
            m = semopy.ModelMeans(desc, intercepts=True)
        r = m.fit(repl, )
        if not r.success:
            continue
        ins = m.inspect(se_robust=True)
        ins = ins[ins.rval == '1']
        vals = ins.Estimate.values
        if (vals[0] > 0) and (vals[1] > 0):
            p1, p2 = ins['p-value'].values.flatten()
            genes[gs[i]] = 1 - (1 - p1) * (1 - p2)
    return genes

results = dict()
for line, hours in dcols.items():
    print(line)
    subcols = [its for its in hours.values()]
    results[line] = test_monotonicity(df, subcols)

# sorted_res = dict()
# for line, its in results.items():
#     sorted_res[line] = [(s, its[s]) for s in sorted(its, key=lambda x: its[x])]
#     if not mp_correction:
#         pvals = [v[1] for v in sorted_res[line]]
#         accept = multipletests(pvals, alpha=0.05)[0]
#         sorted_res[line] = [t for t, a in zip(sorted_res[line], accept) if a]
#     else:
#         sorted_res[line] = [t for t in sorted_res[line] if t[1] < 0.25]

# sts = {l: set([v[0] for v in its]) for l, its in sorted_res.items()}


# def is_monotone(df: pd.DataFrame, cols: list):
#     lt = [df[col].T.mean() for col in cols]
#     res = np.ones(len(df), dtype=bool)
#     for i in range(len(lt) - 1):
#         res &= lt[i] >= lt[i + 1]
#     return pd.Series(res, index=df.index)

# def normalize(df: pd.DataFrame, cols: list, genes: list, geom=False):
#     all_cols = list()
#     for cs in cols:
#         all_cols.extend(cs)
#     df = df[all_cols]
#     if geom:
#         nf = pd.Series(gmean(df.loc[genes]), index=df.columns)
#     else:
#         nf = df.loc[genes].sum(axis=0)
#         nf = nf / (df.sum(axis=0) - nf)
#     nf /= nf[cols[0]].mean()
#     nf /= nf.prod() ** (1 / len(nf))
#     return cpm(df, norm_factors=nf), nf

# viable_genes = None
# for l, its in sts.items():
#     if viable_genes is None:
#         viable_genes = its
#     else:
#         viable_genes &= its
# for line, hours in dcols.items():
#     print(line)
#     subcols = [its for its in hours.values()]
#     base_level = is_monotone(cpm(all_df, norm_factors='TMM'), subcols).sum()
#     best_score = base_level
#     best_result = None
#     for gene in list(viable_genes):
#         ndf, nf = normalize(all_df, subcols, [gene])
#         im = is_monotone(ndf, subcols).sum()
#         if im < base_level:
#             viable_genes.remove(gene)
#         if im > best_score:
#             best_score = im
#             best_result = [gene]
#             print(im, gene)
# base_level = None
# for _, hours in dcols.items():
#     subcols = [its for its in hours.values()]
#     ndf, nf = normalize(all_df, subcols, list(its))
#     if base_level is None:
#         base_level = is_monotone(ndf, subcols)
#     else:
#         base_level &= is_monotone(ndf, subcols)
# base_level = base_level.sum()
# best_score = base_level
# for i in range(1, len(viable_genes)):
#     print(i)
#     vs = list(combinations(viable_genes, i))
#     shuffle(vs)
#     t0 = time()
#     for its in vs:
#         im = None
#         for _, hours in dcols.items():
#             subcols = [its for its in hours.values()]
#             ndf, nf = normalize(all_df, subcols, list(its))
#             if im is None:
#                 im = is_monotone(ndf, subcols)
#             else:
#                 im &= is_monotone(ndf, subcols)
#         im = im.sum()
#         # print(its, im)
#         if im >= best_score:
#             best_score = im
#             best_result = its
#             t0 = time()
#             print(im, its)
#         if time() - t0 > 60:
#             break