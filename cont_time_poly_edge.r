library(edgeR)

args = commandArgs(trailingOnly=TRUE)

if (length(args) < 6)
{
  args = c("results/adjusted_counts.tsv",
           "dd_pYB1",
           "dd",
           "0;4;8",
           "results/edger_ct_nonlinear",
           "1;2;3")
  print("Warning: using default arguments")
}else if (length(args) < 6)
{
  args = c(args, "1;2;3")
}
print("Arguments:"); print(cat(args, sep="|"))

group_control = args[2]
group_test = args[3]
times = rapply(strsplit(args[4], ";"), function(x){paste0(x, "h")})


samples = strsplit(args[6], ";")[[1]]

columns = NULL; lines = NULL; hours = NULL
for (group in c(group_control, group_test))
{
  for (hour in times)
  {
    for (sample in samples)
    {
      col = paste(group, hour, sample, sep="_")
      columns = c(columns, col)
      lines = c(lines, group)
      hours = c(hours, as.numeric(substr(hour, 0, nchar(hour) - 1)))
    }
  }
}

lines = relevel(factor(lines), group_control)
counts = read.table(args[1], row.names=1, sep='\t', header=T)[, columns]
nf = read.table('results/norm_factors.tsv', sep='\t', row.names=1, header=T)[columns, ]
nf = data.frame(as.numeric(nf), row.names=columns)
nf = nf / (prod(nf) ^ (1/dim(nf)[1]))
colnames(nf) = c('norm.factors')
hours_sq = hours ** 2
design = model.matrix(~0 + lines + hours:lines + hours_sq:lines) 
rownames(design) = colnames(counts)
counts = DGEList(counts, group=lines, norm.factors=nf$norm.factors)


counts = estimateDisp(counts, design)

fit = glmQLFit(counts, design)
print(colnames(design))
filename = paste0(paste(args[5], 'full', args[2], args[3], sep='_'), '.tsv')
contrasts = c(1, -1, 1, -1, 1, -1)
print("full")
print(contrasts)
qlf = glmQLFTest(fit, contrast=contrasts)
write.table(topTags(qlf, n=dim(counts)[1]), filename, sep='\t', col.names=NA)

filename = paste0(paste(args[5], 'poly', args[2], args[3], sep='_'), '.tsv')
contrasts = c(0, 0, 1, -1, 1, -1)
print("poly")
print(contrasts)
qlf = glmQLFTest(fit, contrast=contrasts)
write.table(topTags(qlf, n=dim(counts)[1]), filename, sep='\t', col.names=NA)

filename = paste0(paste(args[5], 'linear', args[2], args[3], sep='_'), '.tsv')
contrasts = c(0, 0, 1, -1, 0, 0)
print("linear")
print(contrasts)
qlf = glmQLFTest(fit, contrast=contrasts)
write.table(topTags(qlf, n=dim(counts)[1]), filename, sep='\t', col.names=NA)

filename = paste0(paste(args[5], 'quadratic', args[2], args[3], sep='_'), '.tsv')
contrasts = c(0, 0, 0, 0, 1, -1)
print("quadratic")
print(contrasts)
qlf = glmQLFTest(fit, contrast=contrasts)
write.table(topTags(qlf, n=dim(counts)[1]), filename, sep='\t', col.names=NA)