library(ComplexHeatmap)
library(ggplot2)
df = read.table("results/adjusted_counts.tsv", row.names=1, header=TRUE, sep='\t')
newcols = NULL; groups = NULL
for (c in colnames(df))
{
  t = strsplit(c, "_")[[1]]
  newcols = c(newcols, paste(tail(t, n=2), collapse='_'))
  groups = c(groups, paste(t[1:(length(t)-2)], collapse='_'))
}

cmat = cor(df)
inds = lower.tri(cmat)
cmat[inds] <- NA
colnames(cmat) = newcols
rownames(cmat) = newcols
filename = paste(getwd(), 'results', 'correlation.png', sep='/')
png(filename, width=10,height=10, units="in",res=900)
h = Heatmap(cmat, name=" ",cluster_rows = F, cluster_columns = F,
            row_split=groups, column_split=groups)
draw(h)
dev.off()
batches = c('1', '2', '3', '1', '2', '3', '4', '4', '3', '5', '6', '7', '5',
            '6', '7', '5', '6', '7', '1', '2', '3', '1', '2', '3', '4', '4',
            '3', '5', '6', '7', '5', '6', '7', '5', '6', '7')
lines = c("dd", "dd_pYB1", "dd_pYB1_pYB3", "dd_pYB3")
hours = c("0", "4", "8")
samples = c("1", "2", "3")
i = 1
j = 1
nc = dim(cmat)[1] / length(lines)
filename = paste(getwd(), 'results', 'correlation_batches.png', sep='/')
png(filename, width=10,height=10, units="in",res=900)
draw(h)
for (line in lines)
{
  k = nc
  for (hour in hours)
  {
    for (sample in samples)
    {
      p = k / nc
      shift = 0.5 / nc
      xshift = (i - 1) * 0.18 / nc
      decorate_heatmap_body(" ", {
        grid.text(batches[j], (i - 1)  + (nc - k)/nc + shift + xshift, p - shift)
      }, slice=i)
      k = k - 1
      j = j + 1
    }
  }
  i = i + 1
}
dev.off()