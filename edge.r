library(edgeR)

include_intercept = FALSE
args = commandArgs(trailingOnly=TRUE)

if (length(args) < 6)
{
  args = c("results/adjusted_counts.tsv",
           "dd_pYB1",
           "dd",
           "0;8",
           "-1;1",
           "results/diffex_dd_vs_pYB1.tsv",
           "1;2;3")
  print("Warning: using default arguments")
}else if (length(args) < 7)
{
  args = c(args, "1;2;3")
}
print("Arguments:"); print(cat(args, sep="|"))


group_control = args[2]
group_test = args[3]
times = rapply(strsplit(args[4], ";"), function(x){paste0(x, "h")})
contrasts = rapply(strsplit(args[5], ";"), as.integer)

contrasts = c(contrasts, -contrasts)
samples = strsplit(args[7], ";")[[1]]

columns = NULL; lines = NULL; hours = NULL
for (group in c(group_control, group_test))
{
  for (hour in times)
  {
    for (sample in samples)
    {
      col = paste(group, hour, sample, sep="_")
      columns = c(columns, col)
      lines = c(lines, group)
      hours = c(hours, hour)
    }
  }
}

hours = factor(hours, ordered=T)
lines = relevel(factor(lines), group_control)
counts = read.table(args[1], row.names=1, sep='\t', header=T)[, columns]
nf = read.table('results/norm_factors.tsv', sep='\t', row.names=1, header=T)[columns, ]
nf = data.frame(as.numeric(nf), row.names=columns)
nf = nf / (prod(nf) ^ (1/dim(nf)[1]))
colnames(nf) = c('norm.factors')
groups = lines:hours
if (include_intercept)
{
  design = model.matrix(~groups)
}else
{
  design = model.matrix(~0 + groups) 
}
print(colnames(design))
print(contrasts)
rownames(design) = colnames(counts)
counts = DGEList(counts, group=groups, norm.factors=nf$norm.factors)



counts = estimateDisp(counts, design)
fit = glmQLFit(counts, design)
qlf = glmQLFTest(fit, contrast=contrasts)

write.table(topTags(qlf, n=dim(counts)[1]), args[6], sep='\t', col.names=NA)