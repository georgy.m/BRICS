library(edgeR)

include_intercept = FALSE
args = commandArgs(trailingOnly=TRUE)

if (length(args) < 5)
{
  args = c("results/adjusted_counts.tsv",
           "dd",
           "0;4;8",
           "-1;1;0",
           "results/diffex_dd.tsv",
           "1;2;3")
  print("Warning: using default arguments")
}else if (length(args) < 6)
{
  args = c(args, "1;2;3")
}

print("Arguments:")
print(cat(args, sep="|"))
group_control = args[2]
times = rapply(strsplit(args[3], ";"), function(x){paste0(x, "h")})
contrasts = rapply(strsplit(args[4], ";"), as.integer)

samples = strsplit(args[6], ";")[[1]]

columns = NULL; hours = NULL
for (hour in times)
{
  for (sample in samples)
  {
    col = paste(group_control, hour, sample, sep="_")
    columns = c(columns, col)
    hours = c(hours, hour)
  }
}

hours = factor(hours, ordered=T)
counts = read.table(args[1], row.names=1, sep='\t', header=T)[, columns]
nf = read.table('results/norm_factors.tsv', sep='\t', row.names=1, header=T)[columns, ]
nf = data.frame(as.numeric(nf), row.names=columns)
nf = nf / (prod(nf) ^ (1/dim(nf)[1]))
colnames(nf) = c('norm.factors')
if (include_intercept)
{
  design = model.matrix(~hours)
}else
{
  design = model.matrix(~0 + hours) 
}
rownames(design) = colnames(counts)
counts = DGEList(counts, group=hours, norm.factors=nf$norm.factors)


counts = estimateDisp(counts, design)
fit = glmQLFit(counts, design)
qlf = glmQLFTest(fit, contrast=contrasts)

write.table(topTags(qlf, n=dim(counts)[1]), args[5], sep='\t', col.names=NA)