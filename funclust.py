from sklearn import decomposition as decomp
from sklearn import cluster
from umap import UMAP
from collections import defaultdict
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import datatable as dt
import pandas as pd
import os

def sigmoid(x, beta, h0, h1, t):
    return impulse(x, beta, h0, h1, h1, t, t)

def impulse(x, beta, h0, h1, h2, t1, t2):
    f1 = (h0 + (h1 - h0) / (1 + np.exp(-beta * (x - t1))))
    f2 = (h2 + (h1 - h2) / (1 + np.exp(beta * (x - t2))))
    return f1 * f2 / h1

output = 'results'
seed = 220
np.random.seed(seed)
df = dt.fread(os.path.join(output, 'ImpulseDE2_dd.tsv')).to_pandas()
df = df.set_index(df.columns[0]).sort_values('padj')
prefix = 'imp'
cutoff = 0.05
cols = list(df.columns)
cols = [c for c in cols if c.startswith(prefix + '_')]
df = df.loc[df['padj'] < cutoff, cols]
tdf = df.copy()
# cols = list(filter(lambda c: '_h0' not in c and '_h2' not in c, cols))
df = df[cols]
pca = decomp.PCA(2).fit_transform(df)
umap = UMAP(random_state=seed).fit_transform(df)
t = umap
# t = pca
opt=cluster.DBSCAN(eps=1, min_samples=10).fit(t)
# opt = cluster.OPTICS(min_samples=100).fit(t)
plt.figure()
plt.scatter(t[:, 0], t[:, 1], s=1, c=opt.labels_)
print('Num clusters:', len(np.unique(opt.labels_)))

clusters = defaultdict(list)
for i, c in enumerate(opt.labels_):
    clusters[c].append(i)
    
cl = sorted(clusters, key=lambda x: -len(clusters[x]))
for i, c in enumerate(cl):
    center = t[clusters[c]].mean(axis=0)
    plt.text(center[0], center[1], str(i + 1), alpha=0.5)
plt.title('UMAP of function parameters')
plt.xlabel('Component 1')
plt.ylabel('Component 2')
plt.tight_layout()
# plt.savefig(os.path.join(output, f'{prefix}_umap.png'))
top_n = 10
x = np.linspace(0, 8, 100)
plt.figure(figsize=(10, 16), dpi=200)
for i in range(top_n):
    plt.subplot(int(np.ceil(top_n // 2)), 2, i + 1)
    cnt = 0
    for j in clusters[cl[i]][:10]:
        cnt += 1
        args = list(tdf.iloc[j])
        if prefix == 'sig':
            y = sigmoid(x, *args)
        else:
            y = impulse(x, *args)
        plt.plot(x, y, 'k--')
    # plt.ylim(0, 3000)
    plt.title(f'Cluster #{str(i + 1)} ({len(clusters[cl[i]])})')
plt.tight_layout()
# plt.savefig(os.path.join(output, f'{prefix}_clusts.png'))