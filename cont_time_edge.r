library(edgeR)

args = commandArgs(trailingOnly=TRUE)

if (length(args) < 6)
{
  args = c("results/adjusted_counts.tsv",
           "dd_pYB1",
           "dd",
           "0;4;8",
           "0",
           "results/diffex_dd_vs_pYB1.tsv",
           "1;2;3")
  print("Warning: using default arguments")
}else if (length(args) < 7)
{
  args = c(args, "1;2;3")
}
print("Arguments:"); print(cat(args, sep="|"))

include_intercept = as.logical(as.integer(args[5]))
group_control = args[2]
group_test = args[3]
times = rapply(strsplit(args[4], ";"), function(x){paste0(x, "h")})

if (!include_intercept)
{
  contrasts = c(0, 0, 1, -1)
}else
{
  contrasts = c(1, -1, 1, -1)
}
samples = strsplit(args[7], ";")[[1]]

columns = NULL; lines = NULL; hours = NULL
for (group in c(group_control, group_test))
{
  for (hour in times)
  {
    for (sample in samples)
    {
      col = paste(group, hour, sample, sep="_")
      columns = c(columns, col)
      lines = c(lines, group)
      hours = c(hours, as.numeric(substr(hour, 0, nchar(hour) - 1)))
    }
  }
}

lines = relevel(factor(lines), group_control)
counts = read.table(args[1], row.names=1, sep='\t', header=T)[, columns]
nf = read.table('results/norm_factors.tsv', sep='\t', row.names=1, header=T)[columns, ]
nf = data.frame(as.numeric(nf), row.names=columns)
nf = nf / (prod(nf) ^ (1/dim(nf)[1]))
colnames(nf) = c('norm.factors')

design = model.matrix(~0 + lines + hours:lines) 
rownames(design) = colnames(counts)
counts = DGEList(counts, group=lines, norm.factors=nf$norm.factors)


counts = estimateDisp(counts, design)
fit = glmQLFit(counts, design)
qlf = glmQLFTest(fit, contrast=contrasts)

write.table(topTags(qlf, n=dim(counts)[1]), args[6], sep='\t', col.names=NA)